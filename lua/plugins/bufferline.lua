vim.opt.termguicolors = true
require('bufferline').setup({
  highlights = {
    fill = { bg = '#171717' },
    background = { fg = '#cccccc' },
    buffer_selected = { bold = true, italic = false, bg = '#3a3a3a', fg = '#FF6700' },
    diagnostic_selected = { bold = true },
    info_selected = { bold = true },
    info_diagnostic_selected = { bold = true },
    warning_selected = { bold = true },
    warning_diagnostic_selected = { bold = true },
    error_selected = { bold = true },
    error_diagnostic_selected = { bold = true },
  },
  options = {
    show_close_icon = false,
    --diagnostics = 'nvim_lsp',
    modified_icon = "+",
    numbers = "none",
    show_buffer_icons = false,
    show_buffer_close_icons = false,
    show_close_icon = false,
    max_prefix_length = 8,
    show_tab_indicator = true,
    indicator = {
      style = 'none'
    },
    diagnostics_indicator = function(count, level, diagnostics_dict, context)
      if context.buffer:current() then
        return ''
      end
      if level:match('error') then
        return ' ' .. vim.g.diagnostic_icons.Error
      elseif level:match('warning') then
        return ' ' .. vim.g.diagnostic_icons.Warning
      end
      return ''
    end,
  },
})

